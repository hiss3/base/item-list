import { Component, EventEmitter, Input, Output, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Item } from '../item-list.interface';

@Component({
  selector: 'his-item-header',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './item-header.component.html',
  styleUrls: ['./item-header.component.scss']
})
export class ItemHeaderComponent {

  @Input() item!: Item;
  @Output() delete = new EventEmitter<Item>();
  @Output() update = new EventEmitter<Item>();
  @Output() insert = new EventEmitter<Item>();

  isEdit = false;

  /**
   * 觸發修改事件
   * @param item
   */
  doUpdateItem(item: Item) {

    this.update.emit(item);
  };

  /**
   * 觸發刪除事件
   */
  doDeleteItem() {

    this.delete.emit(this.item);
  };

  /**
   * 觸發新增事件
   * @param item
   */
  doInsertItem(item: Item){

    this.insert.emit(item);
  }

}
