import { Component, EventEmitter, Input, Output, signal } from '@angular/core';
import { Item } from './item-list.interface';
import { ItemHeaderComponent } from './item-header/item-header.component';
import { ItemBodyComponent } from './item-body/item-body.component';
import { NgFor } from '@angular/common';

@Component({
  selector: 'his-item-list',
  standalone: true,
  imports: [ NgFor ,ItemHeaderComponent, ItemBodyComponent ],
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss']
})
export class ItemListComponent {

  @Input() items = signal([] as Item[]);
  @Output() update = new EventEmitter<Item>();
  @Output() delete = new EventEmitter<Item>();

  /**
   * 觸發修改事件
   * @param item
   */
  doUpdateItem(item: Item) {

    this.update.emit(item);
  };

  /**
   * 觸發刪除事件
   */
  doDeleteItem() {

    this.delete.emit();
  };

}
