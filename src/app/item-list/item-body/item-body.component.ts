import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Item } from '../item-list.interface';

@Component({
  selector: 'his-item-body',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './item-body.component.html',
  styleUrls: ['./item-body.component.scss']
})
export class ItemBodyComponent {

  @Input() item!: Item;

}
